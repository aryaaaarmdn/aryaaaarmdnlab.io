gsap.registerPlugin(ScrollTrigger);

function animateHeaderText() {
    const words = ["Aryaa.", "a Student", "a Coder."]

    let cursor = gsap.to('.cursor', { opacity: 0, ease: "power2.inOut", repeat: -1 })
    let masterTl = gsap.timeline({ repeat: -1 }).pause()
    let boxTl = gsap.timeline()

    boxTl.to('.box', { duration: 1, width: "12vmax", delay: 0.5, ease: "power4.inOut" })
        .from('.hi', { duration: 1, y: "9vw", ease: "power3.out" })
        .to('.box', { duration: 1, height: "7vmax", ease: "elastic.out", onComplete: () => masterTl.play() })
        // .to('.box', { duration: 2, autoAlpha: 0.4, yoyo: true, repeat: -1, ease: "rough({ template: none.out, strength:  1, points: 20, taper: 'none', randomize: true, clamp: false})" })
        .to('.box', { duration: 2, autoAlpha: 0.4, yoyo: true, repeat: -1 })

    words.forEach(word => {
        let tl = gsap.timeline({ repeat: 1, yoyo: true, repeatDelay: 1 })
        tl.to('.text', { duration: 1, text: word })
        masterTl.add(tl)
    })
}

window.onload = function () {
    document.body.classList.add('loaded');
    animateHeaderText();
    window.scrollTo(0,0);
}

const reveal = gsap.utils.toArray('.reveal');
reveal.forEach((text, i) => {
    ScrollTrigger.create({
        trigger: text,
        toggleClass: 'active',
        // start: "top 95%",
        // end: "top -5%",
        // markers: true,

    })
});

// profile image Reveal on scroll
let revealContainers = document.querySelectorAll(".image");
let image = document.querySelector("div.image img");
let imageTl = gsap.timeline({
    scrollTrigger: {
        trigger: revealContainers,
        toggleActions: "restart reset restart none"
    }
});

imageTl.set(revealContainers, { autoAlpha: 1 })
    .from(revealContainers, 1.5, {
        xPercent: -100,
        ease: Power2.out
    })
    .from(image, 1.5, {
        xPercent: 100,
        scale: 1.3,
        delay: -1.5,
        ease: Power2.out
    });


// List Animation
const eduContainer = document.querySelector("div.education");
const experienceContainer = document.querySelector("div.experience");

gsap.timeline({
    scrollTrigger: {
        trigger: eduContainer,
        toggleActions: "restart pause resume reset",
        // markers: true,
    }
})
    .from("div.education ul li", {
        opacity: 0,
        x: 200,
        stagger: .5,
        duration: 1,
        ease: "back",
        delay: 1,
    })

gsap.timeline({
    scrollTrigger: {
        trigger: experienceContainer,
        toggleActions: "restart pause resume reset"
    }
}).from("div.experience ul li", {
    opacity: 0,
    x: 200,
    stagger: .5,
    duration: 1,
    ease: "back",
    delay: 1,
})




document.addEventListener('scroll', () => {
    const navbar = document.getElementById('navbar');
    // console.log(`window scroll y ${window.scrollY}`);
    // console.log(`window innerHeight ${window.innerHeight}`)
    // console.log(`document body offset ${document.body.offsetHeight}`)
    // console.log(`innerheight + scroll y ${window.innerHeight + window.scrollY}`)
    if (
        (window.scrollY > window.innerHeight) &&
        !((window.innerHeight + window.pageYOffset) >= document.body.offsetHeight)
    ) {
        // console.log('ok');
        navbar.style.cssText = "bottom:20px; background-color: rgb(19, 18, 18); "
    } else {
        // console.log('ok2')
        navbar.style.cssText = "top:20px; background-color: rgb(19, 18, 18); margin-top:0"
    }

    if (window.scrollY <= 800) navbar.style = null;
})

const navbutton = document.getElementsByClassName("nav-button");
Array.from(navbutton).forEach(btn => {
    btn.addEventListener('click', () => {

        if (document.documentElement.contains(document.querySelector('.active'))) {
            document.querySelector('.active').classList.remove("active");
        }

        btn.classList.add('active');
    })
})